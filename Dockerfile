FROM openjdk:11-jre
ADD target/docker-spring-boot.jar docker-spring-boot.jar
EXPOSE 80
ENTRYPOINT ["java", "-jar", "docker-spring-boot.jar"]
